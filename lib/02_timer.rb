class Timer
  attr_accessor :seconds

  def initialize(seconds = 0)
    @seconds = seconds
  end

  def padded(n)
    return "0#{n}" if n < 10
    "#{n}"
  end

  def time_string
    hours = seconds / 3600
    minutes = (seconds % 3600) / 60
    timer_seconds = seconds % 60
    "#{padded(hours)}:#{padded(minutes)}:#{padded(timer_seconds)}"
  end
end
