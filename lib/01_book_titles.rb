class Book
    attr_reader :title

  def title
    @title
  end

  def title=(new_title)
    lowercase_words = %w{a an the and but or for nor of in}
    @title = new_title.downcase.split.map do |word|
      word.capitalize! unless lowercase_words.include?(word)
      word
    end
    @title[0].capitalize!
    @title = @title.join(' ')
  end
end
