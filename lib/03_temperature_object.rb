
  class Temperature

  def initialize(option = {})
    @option = option
    @fahrenheit = @option[:f]
    @celsius = @option[:c]
  end

  def self.from_celsius(val)
    self.new(c: val)
  end

  def self.from_fahrenheit(val)
    self.new(f: val)
  end

  def fahrenheit=(temp)
    @fahrenheit = temp
  end

  def celsius=(temp)
    @celsius = temp
  end

  def in_fahrenheit
    if @fahrenheit
      @fahrenheit
    else
      ((@celsius * 9) / 5.0) + 32
    end
  end

  def in_celsius
    if @fahrenheit
      ((@fahrenheit - 32) * 5) / 9.0
    else
      @celsius
    end
  end
end

class Celsius < Temperature
  def initialize(temp)
    self.celsius = temp
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    self.fahrenheit = temp
  end
end
